package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Template {
	/**
	 * Unused - helper attribute for associating a template with a stack name
	 */
	@JsonIgnore
	public String stack;

	@JsonProperty("Resources")
	public Map<String, Resource> resources = new HashMap<>();
	@JsonProperty("Outputs")
	public Map<String, Output> outputs = new HashMap<>();
	@JsonProperty("Description")
	public String description;

	public Template setStack(final String name) {
		this.stack = name;
		return this;
	}

	public Template setDescription(final String description) {
		if (description.length() >= 1024)
			throw new AssertionError("Description too long (1024)");
		this.description = description;
		return this;
	}

	public Resource addResource(final Resource resource) {
		resources.put(resource.logicalId, resource);
		return resource;
	}

	public void addOutput(final String id, final StringFunction value) {
		if (!Pattern.compile("^[a-zA-Z0-9]+$").matcher(id).find())
			throw new AssertionError("Output id must be alphanumeric.");
		this.outputs.put(id, new Output(null, value, new Output.Export(id)));
	}

	/**
	 * @return true if no resources defined
	 */
	@JsonIgnore
	public boolean isEmpty() {
		return resources.isEmpty();
	}
}
