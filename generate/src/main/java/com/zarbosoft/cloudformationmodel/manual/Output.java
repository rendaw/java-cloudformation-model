package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Output {
	@JsonProperty(value = "Description")
	public final String description;
	@JsonProperty(value = "Value")
	public final StringFunction value;

	public Output(final String description, final StringFunction value, final Export export) {
		this.description = description;
		this.value = value;
		this.export = export;
	}

	@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
	public static class Export {
		@JsonProperty(value = "Name")
		public final String name;

		public Export(final String name) {
			this.name = name;
		}
	}

	@JsonProperty(value = "Export")
	public final Export export;
}
