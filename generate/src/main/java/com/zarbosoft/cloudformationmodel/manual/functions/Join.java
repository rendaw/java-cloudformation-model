package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@JsonSerialize(using = Join.Serializer.class)
public class Join implements StringFunction {
	public final String delimiter;
	public final List<Object> values = new ArrayList<>();

	public Join(final String delimiter, final Object... values) {
		this.delimiter = delimiter;
		this.values.addAll(Arrays.asList(values));
	}

	public static class Serializer extends JsonSerializer<Join> {
		@Override
		public void serialize(
				final Join value, final JsonGenerator gen, final SerializerProvider serializers
		) throws IOException {
			gen.writeStartObject();
			gen.writeFieldName("Fn::Join");
			gen.writeStartArray();
			gen.writeString(value.delimiter);
			gen.writeStartArray();
			for (final Object o : value.values)
				serializers.defaultSerializeValue(o, gen);
			gen.writeEndArray();
			gen.writeEndArray();
			gen.writeEndObject();
		}
	}
}
