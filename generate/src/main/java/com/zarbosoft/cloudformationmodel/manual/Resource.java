package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Resource {
	@JsonIgnore
	public final String logicalId;
	@JsonProperty(value = "Type")
	public final String type;
	@JsonProperty(value = "Properties")
	public final ResourceProperties properties;
	@JsonProperty(value = "DependsOn")
	final List<String> dependsOn = new ArrayList<>();
	@JsonProperty(value = "Tags")
	final List<Tag> tags = new ArrayList<>();

	public Resource(
			final String logicalId, final ResourceProperties properties
	) {
		this.logicalId = logicalId;
		this.type = properties.getType();
		this.properties = properties;
	}

	public Resource addDependsOn(final String v) {
		this.dependsOn.add(v);
		return this;
	}

	public Resource addDependsOn(final Resource v) {
		this.dependsOn.add(v.logicalId);
		return this;
	}

	public Resource addTag(final String key, final String value) {
		this.tags.add(new Tag().setKey(key).setValue(value));
		return this;
	}

	public Resource addTags(final Map<String, String> tags) {
		tags.forEach(this::addTag);
		return this;
	}
}
