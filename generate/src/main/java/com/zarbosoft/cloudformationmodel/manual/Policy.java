package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Policy {
	@JsonProperty(value = "Statement")
	public List<PolicyStatement> statements = new ArrayList<>();

	@JsonProperty(value = "Version")
	public String version;

	public Policy addStatement(final PolicyStatement statement) {
		this.statements.add(statement);
		return this;
	}

	public Policy addStatements(final Stream<PolicyStatement> statements) {
		statements.forEach(s -> this.statements.add(s));
		return this;
	}

	public Policy setVersion(final String version) {
		this.version = version;
		return this;
	}
}
