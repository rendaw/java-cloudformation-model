package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class PrincipalAWS extends PolicyStatementPrincipal {
	@JsonProperty(value = "AWS")
	public final Object aws;

	public PrincipalAWS(final String arn) {
		this.aws = arn;
	}

	public PrincipalAWS(final StringFunction arn) {
		this.aws = arn;
	}
}
