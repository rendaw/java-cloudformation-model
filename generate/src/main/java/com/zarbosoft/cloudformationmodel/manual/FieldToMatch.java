package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class FieldToMatch {
	@JsonProperty(value = "Type")
	public Object type;

	@JsonProperty(value = "Data")
	public Object data;

	public FieldToMatch setType(String v) {
		this.type = v;
		return this;
	}

	public FieldToMatch setType(StringFunction v) {
		this.type = v;
		return this;
	}

	public FieldToMatch setData(String v) {
		this.data = v;
		return this;
	}

	public FieldToMatch setData(StringFunction v) {
		this.data = v;
		return this;
	}
}
