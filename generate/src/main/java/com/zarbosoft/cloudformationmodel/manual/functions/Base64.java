package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Base64 implements StringFunction {
	@JsonProperty(value = "Fn::Base64")
	Object value;

	public Base64(final StringFunction v) {
		this.value = v;
	}
}
