package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class ImportValue implements StringFunction {
	@JsonProperty(value = "Fn::ImportValue")
	public final String value;

	// TODO make addOutput return output, this takes the Output instead of a string
	public ImportValue(final String value) {
		this.value = value;
	}
}
