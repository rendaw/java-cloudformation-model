package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class PolicyStatement {
	@JsonProperty(value = "Resource")
	public Object resource;
	@JsonProperty(value = "Principal")
	public PolicyStatementPrincipal principal;
	@JsonProperty(value = "Action")
	public List<String> actions = new ArrayList<>();
	@JsonProperty(value = "Effect")
	public String effect;
	@JsonProperty(value = "Condition")
	public PolicyCondition condition;

	public PolicyStatement setResource(final String v) {
		this.resource = v;
		return this;
	}

	public PolicyStatement setResource(final StringFunction v) {
		this.resource = v;
		return this;
	}

	public PolicyStatement setPrincipal(final PolicyStatementPrincipal v) {
		this.principal = v;
		return this;
	}

	public PolicyStatement addAction(final String v) {
		this.actions.add(v);
		return this;
	}

	public PolicyStatement setEffect(final String v) {
		this.effect = v;
		return this;
	}

	public PolicyStatement setCondition(final PolicyCondition condition) {
		this.condition = condition;
		return this;
	}
}
