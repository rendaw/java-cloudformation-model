package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.Resource;
import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

@JsonSerialize(using = GetAtt.Serializer.class)
public class GetAtt implements StringFunction {
	private final Resource resource;
	private final String[] names;

	public GetAtt(final Resource resource, final String... names) {
		this.resource = resource;
		this.names = names.clone();
	}

	public static class Serializer extends JsonSerializer<GetAtt> {
		@Override
		public void serialize(
				final GetAtt value, final JsonGenerator gen, final SerializerProvider serializers
		) throws IOException {
			gen.writeStartObject();
			gen.writeFieldName("Fn::GetAtt");
			gen.writeStartArray();
			gen.writeString(value.resource.logicalId);
			for (final Object o : value.names)
				serializers.defaultSerializeValue(o, gen);
			gen.writeEndArray();
			gen.writeEndObject();
		}
	}
}
