package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Tag {
	@JsonProperty("Key")
	public Object Key_;

	@JsonProperty("Value")
	public Object Value_;

	public Tag() {
	}

	public Tag setKey(final String value) {
		this.Key_ = value;
		return this;
	}

	public Tag setKeyIf(final boolean cond, final String value) {
		if (cond)
			this.Key_ = value;
		return this;
	}

	public Tag setKey(final StringFunction value) {
		this.Key_ = value;
		return this;
	}

	public Tag setKeyIf(final boolean cond, final StringFunction value) {
		if (cond)
			this.Key_ = value;
		return this;
	}

	public Tag setValue(final String value) {
		this.Value_ = value;
		return this;
	}

	public Tag setValueIf(final boolean cond, final String value) {
		if (cond)
			this.Value_ = value;
		return this;
	}

	public Tag setValue(final StringFunction value) {
		this.Value_ = value;
		return this;
	}

	public Tag setValueIf(final boolean cond, final StringFunction value) {
		if (cond)
			this.Value_ = value;
		return this;
	}
}
