package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.function.Consumer;

/**
 * Fluent JSON construction helper
 */
public class JSONBuilder {
	public static JsonNodeFactory f = JsonNodeFactory.instance;

	/**
	 * Start building an object
	 *
	 * @param v Method to define object
	 * @return JSON string
	 */
	public static String o(final Consumer<Object> v) {
		final Object s = new Object();
		v.accept(s);
		return pretty(s.node);
	}

	/**
	 * Start building an object
	 *
	 * @param v Method to define object
	 * @return JSON node
	 */
	public static JsonNode on(final Consumer<Object> v) {
		final Object s = new Object();
		v.accept(s);
		return s.node;
	}

	/**
	 * Start building an array
	 *
	 * @param v Method to define array
	 * @return JSON string
	 */
	public static String a(final Consumer<Array> v) {
		final Array s = new Array();
		v.accept(s);
		return pretty(s.node);
	}

	/**
	 * Start building an array
	 *
	 * @param v Method to define array
	 * @return JSON node
	 */
	public static JsonNode an(final Consumer<Array> v) {
		final Array s = new Array();
		v.accept(s);
		return s.node;
	}

	/**
	 * Helper to pretty print a JSON node
	 *
	 * @param r node to print
	 * @return JSON string
	 */
	public static String pretty(final JsonNode r) {
		final ByteArrayOutputStream o = new ByteArrayOutputStream();
		final ObjectMapper jackson = new ObjectMapper();
		try {
			jackson.writerWithDefaultPrettyPrinter().writeValue(o, r);
		} catch (IOException e) {
			throw new UncheckedIOException(e);
		}
		return o.toString();
	}

	/**
	 * JSON object builder
	 */
	public static class Object {
		ObjectNode node = new ObjectNode(f);

		/**
		 * Create a string object element
		 *
		 * @param key object key
		 * @param v   object value
		 * @return this for chaining
		 */
		public Object v(final String key, final String v) {
			node.set(key, f.textNode(v));
			return this;
		}

		/**
		 * Create a string object element
		 *
		 * @param key object key
		 * @param v   object value
		 * @return this for chaining
		 */
		public Object v(final String key, final int v) {
			node.set(key, f.numberNode(v));
			return this;
		}

		/**
		 * Create a string object element
		 *
		 * @param key object key
		 * @param v   object value
		 * @return this for chaining
		 */
		public Object v(final String key, final double v) {
			node.set(key, f.numberNode(v));
			return this;
		}

		/**
		 * Create a string object element
		 *
		 * @param key object key
		 * @param v   object value
		 * @return this for chaining
		 */
		public Object v(final String key, final boolean v) {
			node.set(key, f.booleanNode(v));
			return this;
		}

		/**
		 * Create an object value
		 *
		 * @param key object key
		 * @param v   Method to define the value
		 * @return this for chaining
		 */
		public Object o(final String key, final Consumer<Object> v) {
			final Object s = new Object();
			v.accept(s);
			node.set(key, s.node);
			return this;
		}

		/**
		 * Create an array value
		 *
		 * @param key object key
		 * @param v   Method to define the value
		 * @return this for chaining
		 */
		public Object a(final String key, final Consumer<Array> v) {
			final Array s = new Array();
			v.accept(s);
			node.set(key, s.node);
			return this;
		}

		/**
		 * Get resultant Jackson JSON node
		 *
		 * @return resultant node
		 */
		JsonNode build() {
			return node;
		}
	}

	/**
	 * JSON array builder
	 */
	public static class Array {
		ArrayNode node = new ArrayNode(f);

		/**
		 * Create an string value
		 *
		 * @param v Value
		 * @return this for chaining
		 */
		public Array v(final String v) {
			node.add(f.textNode(v));
			return this;
		}

		/**
		 * Create an int value
		 *
		 * @param v Value
		 * @return this for chaining
		 */
		public Array v(final int v) {
			node.add(f.numberNode(v));
			return this;
		}

		/**
		 * Create an double value
		 *
		 * @param v Value
		 * @return this for chaining
		 */
		public Array v(final double v) {
			node.add(f.numberNode(v));
			return this;
		}

		/**
		 * Create an boolean value
		 *
		 * @param v Value
		 * @return this for chaining
		 */
		public Array v(final boolean v) {
			node.add(f.booleanNode(v));
			return this;
		}

		/**
		 * Create an object value
		 *
		 * @param v Method to define the value
		 * @return this for chaining
		 */
		public Array o(final Consumer<Object> v) {
			final Object s = new Object();
			v.accept(s);
			node.add(s.node);
			return this;
		}

		/**
		 * Create an array value
		 *
		 * @param v Method to define the value
		 * @return this for chaining
		 */
		public Array a(final Consumer<Array> v) {
			final Array s = new Array();
			v.accept(s);
			node.add(s.node);
			return this;
		}

		/**
		 * Get resultant Jackson JSON node
		 *
		 * @return resultant node
		 */
		JsonNode build() {
			return node;
		}
	}
}
