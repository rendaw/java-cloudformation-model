package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonIgnore;

public abstract class ResourceProperties {
	@JsonIgnore
	public abstract String getType();
}
