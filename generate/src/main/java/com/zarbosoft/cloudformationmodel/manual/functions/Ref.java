package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.Resource;
import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Ref implements StringFunction {
	@JsonProperty(value = "Ref")
	public final String target;

	public Ref(final Resource target) {
		this.target = target.logicalId;
	}
}
