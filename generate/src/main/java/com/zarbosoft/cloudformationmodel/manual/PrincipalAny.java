package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

@JsonSerialize(using = PrincipalAny.Serializer.class)
public class PrincipalAny extends PolicyStatementPrincipal {

	public PrincipalAny() {
	}

	public static class Serializer extends JsonSerializer<PrincipalAny> {
		@Override
		public void serialize(
				final PrincipalAny value, final JsonGenerator gen, final SerializerProvider serializers
		) throws IOException {
			gen.writeString("*");
		}
	}
}
