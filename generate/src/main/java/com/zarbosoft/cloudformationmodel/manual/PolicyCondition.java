package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class PolicyCondition {
	@JsonProperty(value = "ArnEquals")
	public String arnEquals;

	public PolicyCondition setArnEquals(String name) {
		this.arnEquals = name;
		return this;
	}
}
