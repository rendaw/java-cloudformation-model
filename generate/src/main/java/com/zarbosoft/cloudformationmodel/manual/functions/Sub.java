package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class Sub implements StringFunction {
	@JsonProperty(value = "Fn::Sub")
	public final String pattern;

	public Sub(final String pattern) {
		this.pattern = pattern;
	}
}
