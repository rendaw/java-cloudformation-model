package com.zarbosoft.cloudformationmodel.manual.functions;

import com.zarbosoft.cloudformationmodel.manual.StringFunction;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

@JsonSerialize(using = StringString.Serializer.class)
public class StringString implements StringFunction {
	public final String text;

	public StringString(final String text) {
		this.text = text;
	}

	public static class Serializer extends JsonSerializer<StringString> {

		@Override
		public void serialize(
				final StringString value, final JsonGenerator gen, final SerializerProvider serializers
		) throws IOException {
			gen.writeString(value.text);
		}
	}
}
