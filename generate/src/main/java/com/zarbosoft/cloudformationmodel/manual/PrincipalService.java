package com.zarbosoft.cloudformationmodel.manual;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class PrincipalService extends PolicyStatementPrincipal {
	@JsonProperty(value = "Service")
	public final List<String> service = new ArrayList<>();

	public PrincipalService(final String service) {
		this.service.add(service);
	}
}
