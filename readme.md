# An AWS CloudFormation Template Model for Java

* Generated from JSON spec (us-east, but nominally compatible with other regions)
* Fluent!
* Works with Jackson for JSON and YAML serialization
* Enormous!

### Installing

Add the following to your `pom.xml`:

```java
<dependency>
    <groupId>com.zarbosoft</groupId>
    <artifactId>cloudformation-model</artifactId>
    <version>1.0.6</version>
</dependency>
```

### Common Patterns

##### Create a Template

```java
final Template template = new Template();
template.stack = "account";
final Resource billingBucket = template.addResource(new Resource("H0000016692936b39",
        new Bucket().setLifecycleConfiguration(new LifecycleConfiguration().addRules(new Rule()
                .setStatus("Enabled")
                .setExpirationInDays(180)
                .setAbortIncompleteMultipartUpload(new AbortIncompleteMultipartUpload().setDaysAfterInitiation(7))))
));
// Well known id - amazon's billing system
final PrincipalAWS billingPrincipal = new PrincipalAWS("arn:aws:iam::386209384616:root");
template.addResource(new Resource("H0000016692937b09",
        new BucketPolicy()
                .setBucketF(new Ref(billingBucket))
                .setPolicyDocument(new Policy()
                        .addStatement(new PolicyStatement()
                                .setEffect("Allow")
                                .setPrincipal(billingPrincipal)
                                .addAction("s3:GetBucketAcl")
                                .addAction("s3:GetBucketPolicy")
                                .setResource(new Sub(String.format("arn:aws:s3:::${%s}",
                                        billingBucket.logicalId
                                ))))
                        .addStatement(new PolicyStatement()
                                .setEffect("Allow")
                                .setPrincipal(billingPrincipal)
                                .addAction("s3:PutObject")
                                .setResource(new Sub(String.format("arn:aws:s3:::${%s}/*",
                                        billingBucket.logicalId
                                )))))
));
...
```

##### Serialize to JSON

```java
final ByteArrayOutputStream o = new ByteArrayOutputStream();
final ObjectMapper jackson = new ObjectMapper();
jackson.setSerializationInclusion(JsonInclude.Include.NON_NULL);
jackson.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
jackson.writerWithDefaultPrettyPrinter().writeValue(o, template);
```

##### Serialize to YAML

```java
final ByteArrayOutputStream o = new ByteArrayOutputStream();
final YAMLFactory yf = new YAMLFactory();
yf.configure(YAMLGenerator.Feature.LITERAL_BLOCK_STYLE, true);
final ObjectMapper jackson = new ObjectMapper(yf);
jackson.setSerializationInclusion(JsonInclude.Include.NON_NULL);
jackson.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
jackson.writerWithDefaultPrettyPrinter().writeValue(o, template);
```

##### Create or update a stack

```java
final AmazonCloudFormation cloudformation = AmazonCloudFormationClientBuilder
        .standard()
        .withCredentials(credentialsProvider)
        .withRegion(region)
        .build();
boolean stackExists = false;
try {
    if (!cloudformation
            .describeStacks(new DescribeStacksRequest().withStackName(template.stack))
            .getStacks()
            .isEmpty())
        stackExists = true;
} catch (final AmazonCloudFormationException ignored) {
}
if (!stackExists)
    cloudformation.createStack(new CreateStackRequest()
            .withStackName(template.stack)
            .withCapabilities(Capability.CAPABILITY_NAMED_IAM)
            .withTemplateBody(o.toString()));
else
    try {
        cloudformation.updateStack(new UpdateStackRequest()
                .withCapabilities(Capability.CAPABILITY_NAMED_IAM)
                .withStackName(template.stack)
                .withTemplateBody(o.toString()));
    } catch (final AmazonCloudFormationException e) {
        if (e.getStatusCode() != 400)
            throw new RuntimeException(e);
    }
String lastState;
while (true) {
    lastState = cloudformation
            .describeStacks(new DescribeStacksRequest().withStackName(template.stack))
            .getStacks()
            .get(0)
            .getStackStatus();
    if (!Stream
            .of("UPDATE_IN_PROGRESS", "UPDATE_COMPLETE_CLEANUP_IN_PROGRESS", "CREATE_IN_PROGRESS")
            .anyMatch(lastState::equals))
        break;
    Thread.sleep(5000);
}
if (!Stream.of("UPDATE_COMPLETE", "CREATE_COMPLETE").anyMatch(lastState::startsWith))
    throw new RuntimeException(String.format("Stack update failed: %s", lastState));
```

### Notes

* Update the definitions by replacing `generate/src/main/resources/com/zarbosoft/internal/cloudformationmodel_generate/CloudFormationResourceSpecification.json` with a new version from https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/cfn-resource-specification.html

* Policies are not part of the JSON spec so I've put together the parts of that I regularly use. If you need more features, create an issue (with a working JSON/YAML sample, please).
